import axios from "axios";

export const Uploader = () => {

    const handleUpload = async (e) => {
        const file = e.target.files[0];

        const formData = new FormData();

        formData.set('image', file);

        const resp = await axios.post('http://localhost:5000/upload', formData);
        console.log(resp);
    }

    return (
        <div>
            <input type="file" onChange={handleUpload} />
        </div>
    )
}