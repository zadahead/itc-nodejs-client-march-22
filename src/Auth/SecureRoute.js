import { useContext, useEffect, useState } from "react"
import { Link, Navigate, useNavigate } from "react-router-dom";
import { userContext } from "../Context/userContext";


/*
  <SecureRoute>
    <Messages />
  </SecureRoute>  

  if you are logged in, return the child
  if not - navigate to /login
*/

export const SecureRoute = ({ children, role }) => {
  const { isLogin, user } = useContext(userContext);

  if (!isLogin) {
    return <Navigate to='/login' />;
  }

  if (role && user?.permissions && !user.permissions[role]) {
    return <Navigate to='/login' />;
  }

  return children;

}

export const Securelink = ({ children, to, role }) => {
  const { isLogin, user } = useContext(userContext);

  if (!isLogin) {
    return <></>;
  }

  if (role && user?.permissions && !user.permissions[role]) {
    return <></>;
  }

  return <Link to={to}>{children}</Link>;

}