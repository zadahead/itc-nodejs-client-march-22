/*
    1) create 2 buttons inside header "login" and "logout"
    2) "Login" will be display if user if logged out
    3) "Logout" will be display if user if logged in

    4) "Login" will show '<Login />' component
    5) "Logout" will delete the token
*/

import { useContext } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import { Securelink } from './Auth/SecureRoute';
import { userContext } from './Context/userContext';
import './Header.css';

export const Header = () => {
    const { isLogin, logout, user } = useContext(userContext);

    return (
        <>
            <div className='Header'>
                <Routes>
                    <Route path="/login" element={
                        <>
                            <Link to='/home'>Home</Link>
                        </>
                    } />

                    <Route path='*' element={
                        <>
                            {isLogin && (
                                <div className='line'>
                                    <h3>{user?.name}</h3>
                                    <Link to='/message'>My Messages</Link>
                                    <Securelink to='/marketing' role="marketing">Marketing</Securelink>
                                    <button onClick={logout}>logout</button>
                                </div>
                            )}

                            {!isLogin && (
                                <Link to='/login'>Login</Link>
                            )}
                        </>
                    } />
                </Routes>
            </div>
        </>
    )
}