
import "./App.css";
import { Header } from "./Header";
import { Login } from "./Views/Login/Login";
import { Tweets } from "./Tweets";

import { Routes, Route } from "react-router-dom";
import { Home } from "./Views/Home/Home";
import { Profile } from "./Views/Profile/Profile";
import { Messages } from "./Views/Messages/Messages";
import { Message } from "./Views/Message/Message";
import { SecureRoute } from "./Auth/SecureRoute";
import { Marketing } from "./Views/Marketing/Marketing";
import { Uploader } from "./Components/Uploader";

/*
    1) create new route GET /me
    2) /me will return user information (without password)
    3) the client will use that info to display user name (also after reload)
*/

export const App = () => {
    return (
        <div className="App">
            <Header />

            <Uploader />
            <img style={{ width: '200px' }} src="http://localhost:5000/images/sdsd0ca73da5cf492ea46ddf6b487e9aaffc.jpg" alt="asd" />


            <Routes>
                <Route path="/message" element={<SecureRoute><Messages /></SecureRoute>} />
                <Route path="/message/:id" element={<Message />} />
                <Route path='/login' element={<Login />} />
                <Route path='/profile' element={<SecureRoute><Profile /></SecureRoute>} />

                <Route path='/marketing' element={<SecureRoute role="marketing"><Marketing /></SecureRoute>} />

                <Route path='*' element={<Home />} />
            </Routes>

        </div>
    )
}