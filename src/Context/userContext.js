import { createContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { storeIsLoggedIn, storeLogin, storeLogout } from "../Auth/user";

import { fetchUrl } from "../Lib/axios";

export const userContext = createContext({});

const Provider = userContext.Provider;

export const UserProvider = ({ children }) => {
    const navigate = useNavigate();
    const [isLogin, setIsLogin] = useState(storeIsLoggedIn());
    const [user, setUser] = useState({});

    console.log('user', user)

    useEffect(() => {
        if (isLogin) {
            fetchUrl('/users/me')
                .then(u => setUser(u))
                .catch(setUser({}));
        }
    }, [isLogin])

    const login = (token) => {
        storeLogin(token);
        setIsLogin(storeIsLoggedIn());
        navigate('/profile');
    }

    const logout = () => {
        storeLogout();
        setIsLogin(storeIsLoggedIn());
        navigate('/login');
    }

    const value = {
        isLogin,
        login,
        logout,
        user
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}