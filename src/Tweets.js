/*
    1) load <Tweets /> on the app
    2) pass authorization header with the token
    3) set the list, or console log error
*/

import { useEffect, useState } from "react";
import { fetchUrl } from "./Lib/axios";

export const Tweets = () => {
    const [list, setList] = useState();

    useEffect(() => {
        fetchUrl('/tweets').then(list => {
            setList(list);
        }).catch(err => {
            setList([]);
        })
    }, [])

    const renderList = () => {
        return list.map(i => {
            return <h4 key={i.id} >{i.name}</h4>
        })
    }

    return (
        <div>
            <h3>Tweets List</h3>
            {list ? renderList() : <h4>loading...</h4>}
        </div>
    )
}