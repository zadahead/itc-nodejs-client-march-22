import { useEffect, useState } from "react"
import { fetchUrl } from "../../Lib/axios"

export const Messages = () => {
    const [list, setList] = useState(null);

    useEffect(() => {
        fetchUrl('/tweets').then(list => {
            setList(list);
        })
    }, [])

    return (
        <div>
            <h1>Messages</h1>
            {list && (
                list.map(i => (
                    <div key={i.id} >
                        <a href={i.desc}>{i.desc}</a>
                    </div>
                ))
            )}
        </div>
    )
}