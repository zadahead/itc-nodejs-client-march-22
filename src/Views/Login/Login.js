/*
    1) Create a new <Login /> Component
    2) send axios request to login
    3) save access_token on localhost
*/

import axios from "axios";
import { useContext, useState } from "react";
import { userContext } from "../../Context/userContext";

import './Login.css';

export const Login = () => {
    const { login } = useContext(userContext);



    const [email, setEmail] = useState('mosh@gmail.com');
    const [password, setPassword] = useState('aaaaa');

    const handleLogin = async () => {
        try {
            const resp = await axios.post('http://localhost:4000/users/login', {
                email,
                password
            })

            console.log(resp);

            const { access_token } = resp.data.data;

            login(access_token);
        } catch (error) {
            console.log(error.response.data.message);
        }
    }

    return (
        <div className="Login">
            <div>
                <div>
                    <h1>Login</h1>
                </div>
                <div>
                    <input placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div>
                    <input placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div>
                    <button onClick={handleLogin}>Login</button>
                </div>
            </div>
        </div>
    )
}