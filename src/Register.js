import axios from 'axios';
import { useState } from 'react';
import './Register.css';

export const Register = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleRegister = () => {
        console.log(name, email, password);
        axios.post(
            'http://localhost:4000/users/register',
            {
                name,
                email,
                password
            }
        ).then(resp => {
            console.log('valid', resp.data);
        }).catch(err => {
            console.log('error', err.response.data);
        })
        //implement axios.post request to register the user
    }

    return (
        <div className="Register">
            <div>
                <div>
                    <h1>Registration</h1>
                </div>
                <div>
                    <input placeholder="name" value={name} onChange={(e) => setName(e.target.value)} />
                </div>
                <div>
                    <input placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div>
                    <input placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div>
                    <button onClick={handleRegister}>Register</button>
                </div>
            </div>
        </div>
    )
}